#include <iostream>
#include <string>
using namespace std;

int main()
{
	string classes[3];

	classes[0] = "CS 134";
	classes[1] = "CS 200";
	classes[2] = "CS 235";

	for (int i = 0; i < 3; i++)
	{
		cout << (i + 1) << ". " << classes[i] << endl;
	}

	return 0;
}
