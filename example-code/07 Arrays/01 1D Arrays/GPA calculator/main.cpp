#include <iostream>
#include <string>
using namespace std;

int main()
{
    const int COURSE_COUNT = 4;

    string  courses[COURSE_COUNT];
    float   grade[COURSE_COUNT];

    float sum = 0;
    for ( int i = 0; i < COURSE_COUNT; i++ )
    {
        cout << "ITEM " << i << ":" << endl;
        cout << "What was the course? ";
        cin >> courses[i];
        cout << "What was your grade? (0-4): ";
        cin >> grade[i];

        sum += grade[i];
        cout << endl;
    }
    cout << endl << endl;

    float gpa = sum / COURSE_COUNT;

    cout << "COURSE \t\t GRADE" << endl;
    for ( int i = 0; i < COURSE_COUNT; i++ )
    {
        cout << courses[i] << "\t\t" << grade[i] << endl;
    }
    cout << "The GPA is: " << gpa << endl;


    return 0;
}
