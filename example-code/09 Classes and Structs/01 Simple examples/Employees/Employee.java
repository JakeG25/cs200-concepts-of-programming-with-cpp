public class Employee
{
    public String name;
    public double salary;
    public Department department;
    
    public void Setup( String newName, double newSalary, Department newDepartment )
    {
        name = newName;
        salary = newSalary;
        department = newDepartment;
    }
    
    public void Display()
    {
        System.out.println();
        System.out.println( name );
        System.out.println( "\t Salary: " + salary );
        System.out.println( "\t Department: " + department.name );
    }
}
