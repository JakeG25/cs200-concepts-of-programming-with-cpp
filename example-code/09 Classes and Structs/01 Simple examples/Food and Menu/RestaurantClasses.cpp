#include "RestaurantClasses.hpp"

Menu::Menu()
{
	// Initialize items
	breakfastList[0].name = "Egg sandwich";
	breakfastList[0].price = 3.99;
	breakfastList[1].name = "Pancakes";
	breakfastList[1].price = 5.99;
	breakfastList[2].name = "Cereal";
	breakfastList[2].price = 1.99;

	lunchList[0].name = "Tortilla soup";
	lunchList[0].price = 5.99;
	lunchList[1].name = "Boring salad";
	lunchList[1].price = 4.99;
	lunchList[2].name = "Toasty sandwhich";
	lunchList[2].price = 4.99;

	dinnerList[0].name = "Chicken and baked potato";
	dinnerList[0].price = 9.99;
	dinnerList[1].name = "Hamburger and french fries";
	dinnerList[1].price = 5.99;
	dinnerList[2].name = "Alfredo pasta and bread";
	dinnerList[2].price = 11.99;
}

void Menu::Display()
{
	cout << endl << "BREAKFAST" << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << "B " << i << ".\t"
			<< "$" << breakfastList[i].price << "\t"
			<< breakfastList[i].name << endl;
	}

	cout << endl << "LUNCH" << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << "L " << i << ".\t"
			<< "$" << lunchList[i].price << "\t"
			<< lunchList[i].name << endl;
	}

	cout << endl << "DINNER" << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << "D " << i << ".\t"
			<< "$" << dinnerList[i].price << "\t"
			<< dinnerList[i].name << endl;
	}
}

Food* Menu::GetItem(char type, int index)
{
	if (type == 'B')
	{
		return &breakfastList[index];
	}
	else if (type == 'L')
	{
		return &lunchList[index];
	}
	else if (type == 'D')
	{
		return &dinnerList[index];
	}

	return nullptr;
}