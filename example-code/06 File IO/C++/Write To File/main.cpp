#include <fstream>  // file input/output
#include <iostream> // console input/output (cin, cout)
#include <string>
using namespace std;

int main()
{
    ifstream input( "text.txt" );

    string story = "";

    string buffer;
    while ( getline( input, buffer ) )
    {
        story = story + buffer + "\n";
    }

    cout << "STORY:" << endl;
    cout << story << endl;

    input.close();



    ofstream output( "text.txt" );

    string textInput = "";

    output << story;

    cout << "Enter in a string, or type QUIT to stop." << endl;
    while ( textInput != "QUIT" )
    {
        getline( cin, textInput );

        if ( textInput != "QUIT" )
        {
            output << textInput << endl;
        }
    }

    return 0;
}
