#include <iostream>
#include <string>
using namespace std;

int main()
{
    int myNumbers[] = { 1, 2, 3, 4, 5, 6, 7 };

    int* ptrNumber = nullptr;

    for ( int i = 0; i < 7; i++ )
    {
        ptrNumber = &myNumbers[i];

        cout << "Item " << i << ": \n\t Address: " << ptrNumber << ", Value: " << *ptrNumber << endl;
    }

    return 0;
}
