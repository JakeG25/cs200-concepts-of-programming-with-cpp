#include <iostream>
#include <string>
using namespace std;

int main()
{
    int a = 2, b = 4, c = 6;

    int* ptrNumber = nullptr;

    cout << endl << "Point to a..." << endl;
    ptrNumber = &a;
    cout << ptrNumber << " = " << *ptrNumber << endl;

    cout << endl << "Point to b..." << endl;
    ptrNumber = &b;
    cout << ptrNumber << " = " << *ptrNumber << endl;

    cout << endl << "Point to c..." << endl;
    ptrNumber = &c;
    cout << ptrNumber << " = " << *ptrNumber << endl;

    return 0;
}
