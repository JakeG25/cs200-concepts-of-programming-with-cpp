#include <iostream>
using namespace std;

int main()
{
    // Count up
    cout << "WHILE LOOP" << endl;
    int i = 0;                      // Initialization
    while ( i < 20 )                // Condition
    {
        cout << i << " ";
        i++;                        // Update
    }

    cout << endl << endl;
    cout << "FOR LOOP" << endl;
    for ( int k = 0; k < 20; k++ )
    {
        cout << k << " ";
    }
    cout << endl;
    return 0;
}
