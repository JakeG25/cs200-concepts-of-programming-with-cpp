import java.util.Scanner;
import java.util.Random;

public class NumberGuesser
{
    static Scanner input = new Scanner( System.in );
    static Random rand = new Random();
    
    public static void main( String args[] )
    {
        int randomNumber = rand.nextInt( 10 ) + 1; // between 1 and 10
        boolean wonGame = false;

        System.out.println( "GUESS THE NUMBER!" );
        System.out.println( "I'm thinking of a number between 1 and 10!" );

        for ( int chances = 5; chances > 0; chances-- )
        {
            System.out.println( "\n\t You have " + chances + " chances left!" );
            System.out.print( "Guess: " );
            int guess;
            guess = input.nextInt();

            if ( guess > randomNumber )
            {
                System.out.print( "Too high!" );
            }
            else if ( guess < randomNumber )
            {
                System.out.print( "Too low!" );
            }
            else
            {
                System.out.println( "You got it!" );
                wonGame = true;
                break;
            }
        }

        if ( wonGame )
        {
            System.out.println( "YOU WIN!" );
        }
        else
        {
            System.out.println( "You lose :(" );
        }
    }
}


