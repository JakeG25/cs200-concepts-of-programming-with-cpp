import java.util.Scanner;

public class RunningTotal
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );
        
        boolean done = false;

        double total = 0;

        while ( !done )
        {
                System.out.println( "\n\n Current total: " + total );
                System.out.print( "Enter an amount: " );
                double amount = input.nextDouble();

                total += amount;

                System.out.println( "Do another? (yes/no): " );
                String choice = input.next();

                if ( choice.equals( "no" ) )
                {
                        done = true;
                }
        }

        System.out.println();
        System.out.println( "Total: " + total );
    }
}
