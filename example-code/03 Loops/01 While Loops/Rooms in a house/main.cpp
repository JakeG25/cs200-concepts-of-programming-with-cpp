#include <iostream>
using namespace std;

int main()
{
    int room1width, room1length, room1area;
    int room2width, room2length, room2area;
    int room3width, room3length, room3area;

    bool done = false;
    while ( !done )
    {
        cout << "----------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "1. Enter room 1" << endl;
        cout << "2. Enter room 2" << endl;
        cout << "3. Enter room 3" << endl;
        cout << "4. View house information" << endl;
        cout << "5. Quit" << endl;
        cout << endl;

        int choice;
        cout << "Choice: ";
        cin >> choice;

        switch( choice )
        {
        case 1:
            cout << "ROOM 1" << endl;
            cout << "Enter width: ";
            cin >> room1width;
            cout << "Enter length: ";
            cin >> room1length;
            break;

        case 2:
            cout << "ROOM 2" << endl;
            cout << "Enter width: ";
            cin >> room2width;
            cout << "Enter length: ";
            cin >> room2length;
            break;

        case 3:
            cout << "ROOM 3" << endl;
            cout << "Enter width: ";
            cin >> room3width;
            cout << "Enter length: ";
            cin >> room3length;
            break;

        case 4:
        {
            room1area = room1length * room1width;
            room2area = room2length * room2width;
            room3area = room3length * room3width;

            float houseArea = room1area + room2area + room3area;

            cout << "Room 1:"
                 << "\n\t Width: " << room1width
                 << "\n\t Length: " << room1length
                 << "\n\t Area: " << room1area << endl;

            cout << "Room 2:"
                 << "\n\t Width: " << room2width
                 << "\n\t Length: " << room2length
                 << "\n\t Area: " << room2area << endl;

            cout << "Room 3:"
                 << "\n\t Width: " << room3width
                 << "\n\t Length: " << room3length
                 << "\n\t Area: " << room3area << endl;

            cout << endl;
            cout << "House area: " << houseArea << " sqft" << endl;
        }
        break;

        case 5:
            done = true;
            break;
        }
    }

    cout << "Goodbye" << endl;

    return 0;
}
