import java.util.Scanner;

public class Bank
{
    static Scanner input = new Scanner( System.in );
    
    public static void main( String args[] )
    {
        boolean done = false;
        double balance = 0;

        while ( !done ) // done == false
        {
            System.out.println(  "BANK PROGRAM" );
            System.out.println(  "1. Deposit funds" );
            System.out.println(  "2. Withdraw funds" );
            System.out.println(  "3. Check balance" );
            System.out.println(  "4. Exit" );

            int choice;
            System.out.println(  "Choice: " );
            choice = input.nextInt();

            if (choice == 1)
            {
                System.out.println(  "How much do you want to deposit? " );
                double amount;
                amount = input.nextDouble();

                if (amount <= 0)
                {
                    // Invalid!
                    System.out.println(  "You cannot deposit $0 or less!" );
                }
                else
                {
                    // Valid!
                    balance += amount; // balance = balance + amount
                }

            }
            else if (choice == 2)
            {
                System.out.println(  "How much do you want to withdraw? " );
                double amount;
                amount = input.nextDouble();

                if (amount <= 0)
                {
                    System.out.println(  "You cannot withdraw $0 or less!" );
                }
                else if (amount > balance)
                {
                    System.out.println(  "You cannot withdraw more than your balance!" );
                }
                else
                {
                    balance -= amount; // balance = balance - amount;
                }
            }
            else if (choice == 3)
            {
                System.out.println(  "Current balance: " );
                System.out.println(  "$" + balance );
            }
            else if (choice == 4)
            {
                System.out.println(  "Are you SURE you want to quit? (y/n): " );
                String chooseQuit;
                chooseQuit = input.next();
                
                if (chooseQuit.equals( "y" ) || chooseQuit.equals( "Y" ) )
                {
                    done = true;
                }
            }
        }
    }
}
