#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    cout << "TWO ACCOUNTS" << endl;

    float checkingAccount, savingsAccount;

    cout << "Checking account balance: ";
    cin >> checkingAccount;

    cout << "Savings account balance: ";
    cin >> savingsAccount;

    cout << endl;

    if ( checkingAccount < 0 )
    {
        cout << "Checking is overdrawn!" << endl;
    }

    if ( savingsAccount < 0 )
    {
        cout << "Savings is overdrawn!" << endl;
    }

    return 0;
}

