#include <iostream>
using namespace std;

int main()
{
    cout << "Adopt a pet" << endl;
    cout << "B. Bird adoption: $40" << endl;
    cout << "C. Cat adoption: $50" << endl;
    cout << "D. Dog adoption: $100" << endl;

    char choice = 'a';
    cout << "Choose one: ";
    cin >> choice;

    float price = 0;

    switch ( choice )
    {
        case 'B':       // if ( choice == 'B' || choice == 'b' )
        case 'b':
        price = 40;
        cout << "Bird" << endl;
        break;

        case 'C':       // if ( choice == 'C' || choice == 'c' )
        case 'c':
        price = 50;
        cout << "Cat" << endl;
        break;

        case 'D':       // if ( choice == 'D' || choice == 'd' )
        case 'd':
        price = 100;
        cout << "Dog" << endl;
        break;

        default:        // else
        cout << "Invalid choice!" << endl;
    }

//    if ( choice == 'B' || choice == 'b' )
//    {
//        price = 40;
//    }
//    else if ( choice == 'C' || choice == 'c' )
//    {
//        price = 50;
//    }
//    else if ( choice == 'D' || choice == 'd' )
//    {
//        price = 100;
//    }
//    else
//    {
//        cout << "Invalid choice!" << endl;
//    }

    cout << "Price: " << price << endl;


    return 0;
}
