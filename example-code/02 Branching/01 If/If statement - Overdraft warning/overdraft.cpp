#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    float price;    // The price of some item
    float balance;  // The user's account balance

    cout << "What is your account balance: ";
    cin >> balance;

    cout << "How much does the item cost? ";
    cin >> price;

    balance = balance - price;

    if ( balance < 0 )
    {
        cout << "Overdraft!" << endl;
    }

    cout << "Your balance is now: $" << balance << endl;

    return 0;
}

