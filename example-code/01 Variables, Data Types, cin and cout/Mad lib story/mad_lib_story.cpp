#include <iostream>     // cin, cout
#include <string>       // string
using namespace std;

int main()
{
    cout << "MAD LIB STORY" << endl;

    string job;
    cout << "Enter a job (one word): ";
    cin >> job;

    string name;
    cout << "Enter a name: ";
    cin >> name;

    float money;
    cout << "Enter a money amount (dollars.cents, no $): ";
    cin >> money;

    string noun;
    cout << "Enter a noun: ";
    cin >> noun;

    string pronoun;
    cout << "Enter a pronoun (he/she/they form): ";
    cin >> pronoun;

    string job2;
    cout << "Enter another jobs: ";
    cin >> job2;

    int days;
    cout << "Enter an integer: ";
    cin >> days;

    float money2;
    cout << "Enter another money amount (dollars.cents, no $): ";
    cin >> money2;

    string verb;
    cout << "Enter a verb, past tense (-ed): ";
    cin >> verb;

    cout << endl << endl;

    cout << "There once was a " << job << " named " << name << ". "
        << name << " had exactly $" << money << "." << endl;

    cout << name << " said \"There's no way I can afford a " << noun << " with this!\"" << endl;
    cout << "So, " << pronoun << " took on a second job as a " << job2 << " in order to earn more." << endl << endl;
    cout << "After " << days << " days, " << name << " saved up $" << money2 << "." << endl;
    cout << pronoun << " went and bought the " << noun << ", but it " << verb << " as soon as" << endl;
    cout << pronoun << " brought it home." << endl << endl;
    cout << "\"Well, darn.\" " << name << " said." << endl;
    cout << endl << endl;
    cout << "The End" << endl;

    return 0;
}
