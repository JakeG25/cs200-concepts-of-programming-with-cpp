// Library = pre-written code, meant for use in multiple programs
#include <iostream>     // iostream = Input / Output Stream
using namespace std;    // We're using the standard library of C++

int main()          // Main is starting point
{
    // cout = Console Output
    // << = output stream operator
    // endl = new line
    cout << "Area calculating program" << endl;

    cout << endl;

    // Variable declaration:
    // Data type & Name
    float width;
    float length;
    float area;

    // Assignment statements
    // = is the assignment operator
//    width = 15.5;
//    length = 20.5;

    // cin = Console Input
    // >> = input stream operator

    cout << "Please enter the width: ";
    cin >> width;
    cout << "Please enter the length: ";
    cin >> length;

    cout << endl;

    // \t = tab
    cout << "The width is \t" << width << endl;
    cout << "The length is \t" << length << endl;

    // Assignment operator with multiplication
    area = width * length;  // * = multiplication

    cout << "The area is \t" << area << endl;


    return 0;       // program ends here
}
