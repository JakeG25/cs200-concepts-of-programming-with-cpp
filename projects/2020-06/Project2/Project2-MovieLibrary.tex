\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Movie Library}
\newcommand{\laTitle}       {CS 200 Project 2}
\newcounter{question}

\renewcommand{\chaptername}{Part}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont CS 200 Project 2:}~\\
                {\fontsize{2cm}{3cm}\selectfont Movie Library}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{images/title.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                %An overview compiled by Rachel Singh
            }
        }
        
        %\vspace{1cm} \small
        %This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        %\includegraphics{../images/cc-by-88x31.png}
        %~\\~\\
        
        Last updated \today

    \end{titlepage}
    
	\tableofcontents




\chapter{Project instructions}

\begin{itemize}
	\item	Once done, upload all source (.cpp) and header (.hpp/.h) files.
	\item	Don't zip the source files.
	\item	Don't zip the entire folder and upload that. I only want source files.
\end{itemize}

\paragraph{Topics:} ~\\
Functions, arrays, for loops, file input/output, pass by reference

\chapter{Grading breakdown}

	\includegraphics[width=20cm]{Project1-Grading-Breakdown.png}


\chapter{Project 2: Movie Library}

	\section{About: The program}
	
	For this program, the user will be able to enter a list of 10
	movies in their movie library and have the ability to update
	titles, clear out the list, and view the list. The list of
	movie titles will also be saved to a data file and loaded back in
	so that each time the program is run the movies are loaded and
	they can continue where they left off.


\begin{lstlisting}[style=output]
---------------------------------
MOVIE COLLECTION
4 total movie(s)

MAIN MENU
---------
1. Add movie
2. Update movie
3. Clear all movies
4. View all movies
5. Save and quit
(1 - 5) >> 
\end{lstlisting}

	\section{About: Multiple files}
	
	This program has multiple files you will need to download to get started:
	\textbf{main.cpp}, \textbf{functions.hpp}, and \textbf{functions.cpp}.
	
	~\\
	\begin{itemize}
		\item	\textbf{main.cpp}: This is where the \texttt{main()} function is located,
				as well as the main program loop.
				
		\item	\textbf{functions.hpp}:	In C++, \textbf{function declarations} go by themselves
				in header files, which can be saved as .hpp or .h files.
		
		\item	\textbf{functions.cpp}:	In C++, \textbf{function definitions} go in source files,
				which are .cpp files.
	\end{itemize}
	
	Nothing needs to be changed in the header file, but you will need to make changes within
	main.cpp and functions.cpp.
	
	\newpage
	First, create a new project in Visual Studio, Code::Blocks, or whatever IDE you're using.
	After the project is created, move these source files (.cpp and .hpp files) into your
	project directory.
	
	\section{Adding existing files in Visual Studio}
	
	\begin{enumerate}
		\item	Right-click on your project in the \textbf{Solution Explorer} space.
		\item	Go to \textbf{Add $>$} and then \textbf{Add existing files...}
		\item	Select all the source code files that you downloaded (.cpp and .hpp) and
				should have saved into the project directory. You can hold CTRL while clicking each file
				to select all of them. Then click \textbf{Add}.
	\end{enumerate}
	
	\newpage
	\section{Adding existing files in Code::Blocks}
	
	\begin{enumerate}
		\item	Right-click on your project in the \textbf{Projects} space. Select \textbf{Add files...} ~\\
				\includegraphics{images/codeblocks-addexistingfile.png}
		\item	Locate the three downloaded files that should have been placed in your project folder.
				You can select them all by holding down CTRL and clicking each one. Then click \textbf{Open}. ~\\
				\includegraphics[width=8cm]{images/codeblocks-addexistingfile2.png}
		\item	It will ask the targets to include. Make sure \textbf{Debug} and \textbf{Release} are both selected and hit \textbf{OK}. ~\\ ~\\
				\includegraphics[width=8cm]{images/codeblocks-addexistingfile3.png}
	\end{enumerate}
	
	\newpage
	\section{Overview: main()}
	
	\paragraph{Declarations:}
	In the \textbf{main.cpp} file, the main program has already been written.
	There are three important variables at the beginning of main:
	
\begin{lstlisting}[style=code]
const int MAX_MOVIES = 10;
string movieList[MAX_MOVIES];
int savedMovies = 0;
\end{lstlisting}

	\begin{center}
		\begin{tabular}{p{3cm} p{3cm} p{6cm}}
			\textbf{Name} 			& \textbf{Type} & \textbf{Description} \\ \hline
			\texttt{MAX\_MOVIES}	& const int 	& 	The maximum total amount of movies
														that the program is able to store. \\ \hline
			
			\texttt{movieList}		& string array of size \texttt{MAX\_MOVIES} & 
														A list of movie titles. \\ \hline

			\texttt{savedMovies}	& int			& 	The total amount of movies the user has saved so far.
		\end{tabular}
	\end{center}
	
	\begin{hint}{MAX\_MOVIES vs. savedMovies?}
	At first it might be confusing that there are two integers storing ``size''
	values related to our movie list array. Why is one 10 and one is 0? Well,
	we need both of these integers for a reason.
	
	\paragraph{MAX\_MOVIES:} In C++, we can't resize a normal array after we've declared it.
	Because of this, we're going to have to estimate how much data we might need 
	to store in our program and ``overshoot'' the maximum size. This is what
	the \texttt{MAX\_MOVIES} named constant is for - the total amount of ``slots''
	we have to store movies.
	
	\paragraph{savedMovies:} When the program starts for the first time,
	the total amount of movies the user has saved will be 0. As the user
	adds each movie, \texttt{savedMovies} is added to by 1 each time.
	This is the variable for how many movies the user has actually \textit{added}
	to the list, instead of the maximum possible they \textit{could} add.
	
	\paragraph{The array is full} when \texttt{savedMovies} and \texttt{MAX\_MOVIES}
	have the same value. If \texttt{MAX\_MOVIES} is 10, then the array is full
	once \texttt{savedMovies} equals 10.
	\end{hint}
	
	\newpage
	\paragraph{Save and Load:} At the top of main(), after the variable
	declarations and before the program loop (\texttt{while ( !done )}),
	there is a \textbf{function call} to the \texttt{LoadMovies} function.
	This function has been declared in \textbf{functions.hpp} and
	defined in \textbf{functions.cpp}, but is currently empty.
	
	~\\
	Load is called here before the program loop so that program data will
	be loaded before the main menu starts.
	
	~\\
	Below the program loop (after the while loop's closing curly brace \}),
	there is a \textbf{function call} to the \texttt{SaveMovies} function.
	This is so that the program saves all the movie data \textit{before}
	the program quits, but after the program loop is done running.
	This function is also currently empty and will need to be implemented later.
	
	\paragraph{Inside the program loop:}
	Inside the program loop (The \texttt{while ( !done )}) a main menu
	will be displayed and the user's selection will be stored. Based on
	what the user selects, a function will be called corresponding
	to the feature.
	
	\begin{enumerate}
		\item	Add a movie
		\item	Update a movie
		\item	Clear all movies
		\item	View all movies
	\end{enumerate}
	
	The \textbf{function calls} have already been written, though the
	function definitions for these are currently blank.
	
	\paragraph{All the function calls:}
	
	Notice that each of these functions take several input arguments.
	All of them need the \texttt{movieList} and \texttt{savedMovies} count,
	and only a couple of them will need \texttt{MAX\_MOVIES}.
	
\begin{lstlisting}[style=code]
LoadMovies		( movieList, savedMovies );
AddMovie		( movieList, savedMovies, MAX_MOVIES );
UpdateMovie		( movieList, savedMovies );
ClearAllMovies	( movieList, savedMovies, MAX_MOVIES );
ViewAllMovies	( movieList, savedMovies );
SaveMovies		( movieList, savedMovies );
\end{lstlisting}

	\newpage
	\section{AddMovie function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int reference & Total amount of movies added by user \\ \hline
			\texttt{MAX\_MOVIES} & const int & Maximum amount of movies that can be added.
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:} This function will first check to see if
	the movie list is already full. If it is full, no more movies can be
	added so we will need to display an error message. If the array
	is not full, then we can go ahead and ask the user to enter a movie title
	and then store it in the array.
	
	\paragraph{Steps:}
	\begin{enumerate}
		\item	If the array is full... ~\\
				(We can tell the array is full if \texttt{savedMovies},
				the total amount of movies the user has entered, is equal to
				\texttt{MAX\_MOVIES}, the total ``slots'' available for movies.)
		\begin{enumerate}
			\item	Display an error message, ``Cannot add any new movies! The list is full!''
		\end{enumerate}
				
		\item	Else if the array is not full...
		\begin{enumerate}
			\item	Declare a string named \texttt{movieTitle} where we will store user input.
			\item	Display a message, ``Please enter the movie title:''
			\item	Use \texttt{cin.ignore();} to clear the input buffer.
			\item	Use \texttt{getline( cin, movieTitle );} to get a movie title from the user.
			\item	Copy the movie title into the next available slot in the array. ~\\
					(The next available space will always be at the \texttt{savedMovies} position...
					\texttt{movieList[savedMovies] = movieTitle;} see next page for more info.)
			\item	Increment the \texttt{savedMovies} counter by 1.
			\item	Display a message, ``Movie added.''
		\end{enumerate}
	\end{enumerate}
	
	\begin{hint}{Adding to the end of the array?}
		Whenever we have an array in C++ we will also want to keep a corresponding
		\texttt{int} variable that keeps track of how many items we've stored in the array.
		In this case, we're using \texttt{savedMovies} as our counter.
		~\\
		
		These counter variables begin at 0 when the array is empty...
		
		\begin{center}
			\begin{tikzpicture}
				\draw (0,0) -- (5,0) -- (5,1) -- (0,1) -- (0,0);
				\draw (1,0) -- (1,1); \draw (2,0) -- (2,1);
				\draw (3,0) -- (3,1); \draw (4,0) -- (4,1);
				\node at (-1, 0.5) {Elements};
				\node at (2, -1.5) {savedMovies = 0};
				\node at (-1, -0.5) {Index};
				\node at (0.5, -0.5) {0};
				\node at (1.5, -0.5) {1};
				\node at (2.5, -0.5) {2};
				\node at (3.5, -0.5) {3};
				\node at (4.5, -0.5) {4};
			\end{tikzpicture}
		\end{center}
		
		The first item we add to our array will go at index 0, and then we would increment \texttt{savedMovies} by 1.
		
		\begin{center}
			\begin{tikzpicture}
				\draw (0,0) -- (5,0) -- (5,1) -- (0,1) -- (0,0);
				\draw (1,0) -- (1,1); \draw (2,0) -- (2,1);
				\draw (3,0) -- (3,1); \draw (4,0) -- (4,1);
				\node at (-1, 0.5) {Elements};
				\node at (2, -1.5) {savedMovies = 1};
				\node at (-1, -0.5) {Index};
				\node at (0.5, -0.5) {0};
				\node at (0.5, 0.5) {``A''};
				\node at (1.5, -0.5) {1};
				\node at (2.5, -0.5) {2};
				\node at (3.5, -0.5) {3};
				\node at (4.5, -0.5) {4};
			\end{tikzpicture}
		\end{center}
		
		With one item in the list, the next position we would add a new item at is also index 1.
		And when we have two items in the list...
		
		\begin{center}
			\begin{tikzpicture}
				\draw (0,0) -- (5,0) -- (5,1) -- (0,1) -- (0,0);
				\draw (1,0) -- (1,1); \draw (2,0) -- (2,1);
				\draw (3,0) -- (3,1); \draw (4,0) -- (4,1);
				\node at (-1, 0.5) {Elements};
				\node at (2, -1.5) {savedMovies = 2};
				\node at (-1, -0.5) {Index};
				\node at (0.5, -0.5) {0};
				\node at (0.5, 0.5) {``A''};
				\node at (1.5, -0.5) {1};
				\node at (1.5, 0.5) {``B''};
				\node at (2.5, -0.5) {2};
				\node at (3.5, -0.5) {3};
				\node at (4.5, -0.5) {4};
			\end{tikzpicture}
		\end{center}
		
		... Then the next element we would add to the array would go at position 2.
		As we keep track of adding to the array, \texttt{savedMovies} will store
		the total amount of movies stored, as well as the index position where we
		will add our next item.
	\end{hint}
	
	\newpage
	\section{ViewAllMovies function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int & Total amount of movies added by user \\ \hline
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:} Use a \textbf{for loop} to iterate over all the movies in the list.
	You should be outputting each element's \textbf{index} as well as the \textbf{movie title}.
	
	\hrulefill
	\section{UpdateMovie function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int & Total amount of movies added by user \\ \hline
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:}
	Display a list of all movies and their array indices (i.e., you can call \texttt{ViewAllMovies}
	to do this for you), then ask the user to enter the index of which movie they want to edit.
	Once they select a movie, have them enter a new title for it.
	
	\begin{hint}{cin.ignore()}
		Since you are going from using \texttt{cin >>} to get the index of a movie and
		\texttt{getline()} to get the title of the movie, you will need to call
		\texttt{cin.ignore();} after the \texttt{cin >>} and before the \texttt{getline()}.
	\end{hint}
	
	\newpage
	\section{ClearAllMovies function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int reference & Total amount of movies added by user \\ \hline
			\texttt{MAX\_MOVIES} & const int & Maximum amount of movies that can be added.
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:}
	Use a for loop to iterate through all the movies in the array and
	set the title to empty strings (\texttt{""}). Also set the \texttt{savedMovies} to 0.
	
	\begin{hint}{Lazy deletion}
	The term ``lazy deletion'' refers to when a computer program marks something
	as deleted but without actually removing the data. For example, if we just set
	\texttt{savedMovies} to 0, then the data is still there but will eventually be overwritten
	once new movies are added. Lazy deletion is faster than a thorough delete,
	which requires going over all the old items and removing/replacing them.
	\end{hint}
	
	\hrulefill
	\section{SaveMovies function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int & Total amount of movies added by user \\ \hline
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:}
	Create an \texttt{ofstream} object and open a file, such as ``movies.txt''.
	Use a for loop to iterate through all the movies and display each movie title
	so that one line = one movie.
	
	\newpage
	\section{LoadMovies function}
	
	\paragraph{Parameters:}
	\begin{center}
		\begin{tabular}{ p{3cm} p{3cm} p{7cm} }
			\textbf{Name} & \textbf{Data type} & \textbf{Description} \\ \hline
			\texttt{movieList} & string array & List of movie titles \\ \hline
			\texttt{savedMovies} & int reference & Total amount of movies added by user \\ \hline
		\end{tabular}
	\end{center}
	
	\paragraph{Functionality:}
	Create an \texttt{ifstream} object and open your movies file, such as ``movies.txt''.
	Use \texttt{getline()} with your ifstream object to read in movie titles from
	the text file. Make sure to increment \texttt{savedMovies} for each movie loaded.
	
	\newpage
	\section{Example output}
	\paragraph{Adding a movie:} ~\\
\begin{lstlisting}[style=output]
---------------------------------
MOVIE COLLECTION
4 total movie(s)

MAIN MENU
---------
1. Add movie
2. Update movie
3. Clear all movies
4. View all movies
5. Save and quit
(1 - 5) >> 1

New movie title: Arrival
Movie added.

 Press ENTER to continue...
\end{lstlisting}

	\newpage
	\paragraph{Updating a movie:} ~\\
\begin{lstlisting}[style=output]
MAIN MENU
---------
1. Add movie
2. Update movie
3. Clear all movies
4. View all movies
5. Save and quit
(1 - 5) >> 2

0. Adrenaline Drive
1. The Lost Skeleton of Cadavra
2. My Neighbor Totoro
3. Kiki's Delivery Service
4. Arrival

Update which movie?
(0 - 4) >> 1

Update "The Lost Skeleton of Cadavra"
New title: The Lost Skeleton of Cadavra Returns Again

Movie updated.
\end{lstlisting}

	\newpage
	\paragraph{View all movies:} ~\\
\begin{lstlisting}[style=output]
---------------------------------
MOVIE COLLECTION
5 total movie(s)

MAIN MENU
---------
1. Add movie
2. Update movie
3. Clear all movies
4. View all movies
5. Save and quit
(1 - 5) >> 4

0. Adrenaline Drive
1. The Lost Skeleton of Cadavra Returns Again
2. My Neighbor Totoro
3. Kiki's Delivery Service
4. Arrival
\end{lstlisting}
	
	
	\paragraph{Clear all movies:} ~\\
\begin{lstlisting}[style=output]
---------------------------------
MOVIE COLLECTION
5 total movie(s)

MAIN MENU
---------
1. Add movie
2. Update movie
3. Clear all movies
4. View all movies
5. Save and quit
(1 - 5) >> 3

All movies cleared.
\end{lstlisting}
	

	
\end{document}

