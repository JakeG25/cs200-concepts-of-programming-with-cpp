#ifndef _POST_HPP
#define _POST_HPP

#include <string>
using namespace std;

class Post
{
    public:
    Post();
    Post( string authorName, string text );

    void Setup( string authorName, string text );
    void Display() const;

    string GetText() const;
    string GetAuthor() const;

    private:
    string m_text;
    string m_author;
};

#endif
