#include <iostream>
#include <string>
using namespace std;

int main()
{
    // Game initialization
    int hunger      = 0;
    int health      = 100;
    int happiness   = 100;
    string petName  = "";
    int menuChoice;

    cout << "Enter your pet's name (all one word): ";
    cin >> petName;

    cout << endl;

    // Gameplay Loop
    bool isDone = false;
    while ( !isDone )
    {
        cout << endl;
        cout << "--------------------------------------------------------" << endl;
        cout << petName << endl
            << "\t Hunger: " << hunger << "%"
            << "\t Health: " << health << "%"
            << "\t Happiness: " << happiness << "%" << endl;
        cout << "--------------------------------------------------------" << endl;
        cout    << "OPTIONS: 1. Feed  2. Play  3. Vet  4. Quit" << endl;
        cout << "> ";
        cin >> menuChoice;

        if ( menuChoice == 1 )      // Feed
        {
            cout << endl << "FOODS: 1. Pizza  2. Broccoli  3. Tuna" << endl;
            cout << "> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )      // Pizza
            {
                health -= 1;
                hunger -= 15;
                cout << endl << "You feed pizza to " << petName << endl;
            }
            else if ( menuChoice == 2 ) // Broccoli
            {
                hunger -= 10;
                health += 1;
                cout << endl << "You feed broccoli to " << petName << endl;
            }
            else if ( menuChoice == 3 ) // Tuna
            {
                hunger -= 12;
                health += 2;
                cout << endl << "You feed tuna to " << petName << endl;
            }
        }
        else if ( menuChoice == 2 ) // Play
        {
            cout << endl << "GAMES: 1. Fetch  2. Tug-of-war  3. Videogame" << endl;
            cout << "> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )      // Fetch
            {
                happiness += 8;
                health += 2;
                cout << endl << "You play fetch with " << petName << endl;
            }
            else if ( menuChoice == 2 ) // Tug-of-war
            {
                happiness += 9;
                cout << endl << "You play tug-of-war with " << petName << endl;
            }
            else if ( menuChoice == 3 ) // Videogame
            {
                happiness += 10;
                health -= 1;
                cout << endl << "You play videogames with " << petName << endl;
            }
        }
        else if ( menuChoice == 3 ) // Vet
        {
            cout << endl << "The vet says:" << endl;
            if ( happiness < 50 ) {
                cout << "- Make sure you play with " << petName << endl;
            }
            if ( hunger > 50 ) {
                cout << "- Make sure to feed " << petName << "!!" << endl;
            }
            if ( health < 50 ) {
                cout << "- " << petName << " isn't looking healthy. Take better care of it!" << endl;
            }

            if ( happiness >= 50 && hunger <= 50 && health >= 50 ) {
                cout << petName << " is looking OK." << endl;
            }
        }
        else if ( menuChoice == 4 ) // Quit
        {
            cout << endl << "Are you sure you want to quit?" << endl;
            cout << "1. QUIT \t 2. Don't quit" << endl;
            cout << "> ";
            cin >> menuChoice;

            if ( menuChoice == 1 ) {
                isDone = true;
            }
        }

        hunger += 5;

        if ( hunger > 50 )
        {
            happiness -= 10;
            health -= 10;
        }
        else
        {
            happiness -= 5;
        }

        if      ( happiness < 0 )   { happiness = 0; }
        else if ( happiness > 100 ) { happiness = 100; }

        if      ( hunger < 0 )   { hunger = 0; }
        else if ( hunger > 100 ) { hunger = 100; }

        if      ( health > 100 ) { health = 100; }
        else if ( health < 0 )
        {
            cout << endl;
            cout << "You haven't taken care of " << petName << "!" << endl;
            cout << petName << " has been removed from your care." << endl;
            isDone = true;
        }
    }

    return 0;
}
