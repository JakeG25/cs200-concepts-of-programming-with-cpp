#ifndef _HOUSE_HPP
#define _HOUSE_HPP

#include "Room.hpp"

class House
{
    public:
    House();
    ~House();

    bool IsFull();
    bool IsValidIndex( int index );
    int GetRoomCount();
    void AddRoom();
    void UpdateRoom();
    float GetTotalSquareFootage();
    void Display();

    private:
    const int MAX_ROOMS;
    int m_totalRooms;
    Room m_roomArray[10];

    void SaveHouse();
    void LoadHouse();
};

#endif
