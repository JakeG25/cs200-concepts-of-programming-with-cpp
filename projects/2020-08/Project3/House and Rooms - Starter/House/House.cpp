#include "House.hpp"

#include <iostream>
#include <fstream>
using namespace std;

House::House()
    : MAX_ROOMS( 10 )
{
    m_totalRooms = 0;
    LoadHouse();
}

House::~House()
{
    SaveHouse();
}

bool House::IsFull()
{
    // TODO: Implement me
    return false; // TEMP: Delete me!
}

bool House::IsValidIndex( int index )
{
    // TODO: Implement me
    return false; // TEMP: Delete me!
}

int House::GetRoomCount()
{
    // TODO: Implement me
    return -1; // TEMP: Delete me!
}

void House::AddRoom()
{
    // TODO: Implement me
}

float House::GetTotalSquareFootage()
{
    // TODO: Implement me
    return -1; // TEMP: Delete me!
}

void House::Display()
{
    // TODO: Implement me
}

void House::UpdateRoom()
{
    // TODO: Implement me
}

void House::SaveHouse()
{
    // This is already implemented
    ofstream output( "house.txt" );
    for ( int i = 0; i < m_totalRooms; i++ )
    {
        output << "ROOM_BEGIN" << endl;
        output  << "NAME" << endl
                << m_roomArray[i].GetName() << endl;
        output  << "WIDTH" << endl
                << m_roomArray[i].GetWidth() << endl;
        output  << "LENGTH" << endl
                << m_roomArray[i].GetLength() << endl;
        output  << "HEIGHT" << endl
                << m_roomArray[i].GetHeight() << endl;
        output  << "ROOM_END" << endl << endl;
    }
}

void House::LoadHouse()
{
    // This is already implemented
    ifstream input( "house.txt" );

    string buffer;
    string name;
    int width, length, height;

    while ( getline( input, buffer ) )
    {
        if ( buffer == "NAME" )
        {
            getline( input, name );
        }
        else if ( buffer == "WIDTH" )
        {
            input >> width;
        }
        else if ( buffer == "LENGTH" )
        {
            input >> length;
        }
        else if ( buffer == "HEIGHT" )
        {
            input >> height;
        }
        else if ( buffer == "ROOM_END" )
        {
            m_roomArray[ m_totalRooms ].Setup( name, width, length, height );
            m_totalRooms++;
        }
    }
}
