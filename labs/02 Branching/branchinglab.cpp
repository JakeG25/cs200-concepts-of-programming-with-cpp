#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()
{
    bool done = false;
    while ( !done )
    {
        cout << "Hometown" << endl;
        // .etc.

        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
