\contentsline {chapter}{\numberline {1}Introduction}{2}%
\contentsline {chapter}{\numberline {2}Setting up Source Control}{3}%
\contentsline {section}{\numberline {2.1}GitLab Account - Source hosting website}{3}%
\contentsline {section}{\numberline {2.2}Creating your course repository}{3}%
\contentsline {chapter}{\numberline {3}Setting up an IDE}{5}%
\contentsline {chapter}{\numberline {4}Creating a project and source file}{6}%
\contentsline {chapter}{\numberline {5}Uploading your files to GitLab}{7}%
\contentsline {chapter}{\numberline {6}Submitting exercise link to Canvas}{8}%
\contentsline {chapter}{\numberline {7}Setting up your IDE}{11}%
\contentsline {chapter}{\numberline {8}Using Visual Studio}{13}%
\contentsline {section}{\numberline {8.1}Downloading Visual Studio}{13}%
\contentsline {section}{\numberline {8.2}Installing Visual Studio}{14}%
\contentsline {section}{\numberline {8.3}Creating a program}{15}%
\contentsline {section}{\numberline {8.4}Writing a program}{18}%
\contentsline {section}{\numberline {8.5}Building and running the program}{19}%
\contentsline {section}{\numberline {8.6}Locating your source file}{21}%
\contentsline {chapter}{\numberline {9}Using Code::Blocks}{23}%
\contentsline {section}{\numberline {9.1}Downloading Code::Blocks}{23}%
\contentsline {section}{\numberline {9.2}Installing Code::Blocks}{24}%
\contentsline {section}{\numberline {9.3}Creating a program}{24}%
\contentsline {paragraph}{Project type selection screen:}{26}%
\contentsline {paragraph}{Project information screen:}{26}%
\contentsline {paragraph}{Compiler information screen:}{27}%
\contentsline {section}{\numberline {9.4}Writing a program}{30}%
\contentsline {section}{\numberline {9.5}Building and running the program}{31}%
\contentsline {section}{\numberline {9.6}Locating your source file}{33}%
