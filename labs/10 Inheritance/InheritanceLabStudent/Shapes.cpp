#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
}

void Rectangle::DisplayArea()
{
}

float Rectangle::CalculateArea()
{
    return 0; // placeholder
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
}

void Circle::DisplayArea()
{
}

float Circle::CalculateArea()
{
    return 0; // placeholder
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
