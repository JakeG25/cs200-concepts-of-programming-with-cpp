#include "search.hpp"
#include "sort.hpp"
#include "parser.hpp"
#include "menu.hpp"

int main()
{
    vector<Animal> animals;

    LoadCsvData( "data/Animal_Services_Intake_Data.csv", animals );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Main Menu" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Sort animals",
            "Search for animal by ID"
        } );

        if ( choice == 1 )
        {
            Menu::Header( "Sort animal IDs" );

            int sortChoice = Menu::ShowIntMenuWithPrompt( {
                "Insertion sort",
                "Selection sort",
                "Merge sort"
            } );

            if ( sortChoice == 1 )
            {
                Timer timer;
                timer.Start();
                cout << "Beginning Insertion Sort..." << endl;
                InsertionSort( animals );
                cout << "* Sort completed (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;
            }
            else if ( sortChoice == 2 )
            {
                Timer timer;
                timer.Start();
                cout << "Beginning Selection Sort..." << endl;
                SelectionSort( animals );
                cout << "* Sort completed (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;
            }
            else
            {
                Timer timer;
                timer.Start();
                cout << "Beginning Merge Sort..." << endl;
                MergeSort( animals, 0, animals.size() - 1 );
                cout << "* Sort completed (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;
            }
        }
        else if ( choice == 2 )
        {
            Menu::Header( "Search for animal by ID" );
            string id = Menu::GetStringChoice( "Enter the Animal ID" );

            int searchChoice = Menu::ShowIntMenuWithPrompt( {
                "Linear search",
                "Binary search"
            } );

            int foundIndex = -1;

            if ( searchChoice == 1 )
            {
                Timer timer;
                timer.Start();
                cout << "Beginning Linear Search..." << endl;
                foundIndex = LinearSearch( id, animals );
                cout << "* Search completed with index " << foundIndex << " (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;

                if ( foundIndex != -1 )
                {
                    animals[ foundIndex ].Display();
                }
            }
            else
            {
                cout << "WARNING! This search won't work with unsorted data!" << endl;

                Timer timer;
                timer.Start();
                cout << "Beginning Binary Search..." << endl;
                foundIndex = BinarySearch( id, animals );
                cout << "* Search completed with index " << foundIndex << " (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;

                if ( foundIndex != -1 )
                {
                    animals[ foundIndex ].Display();
                }
            }
        }
    }

    return 0;
}
