#include "animal.hpp"

#include <iostream>
using namespace std;

void Animal::Display()
{
    cout << endl;
    cout << "Animal ID:         " << id << endl
         << "Shelter:           " << shelter << endl
         << "Intake date:       " << intakeDate << endl
         << "Intake type:       " << intakeType << endl
         << "Intake condition:  " << intakeCondition << endl
         << "Animal type:       " << type << endl
         << "Group:             " << group << endl
         << "Breed:             " << breed << endl;
    cout << endl;
}

Animal& Animal::operator=( const Animal& other )
{
    this->id = other.id;
    this->breed = other.breed;
    this->type = other.type;
    return *this;
}

bool operator==( Animal& a1, Animal& a2 )
{
    return ( a1.id == a2.id );
}

bool operator!=( Animal& a1, Animal& a2 )
{
    return !( a1.id == a2.id );
}

bool operator<( Animal& a1, Animal& a2 )
{
    return ( a1.id < a2.id );
}

bool operator>( Animal& a1, Animal& a2 )
{
    return ( a1.id > a2.id );
}

bool operator<=( Animal& a1, Animal& a2 )
{
    return ( a1.id <= a2.id );
}

bool operator>=( Animal& a1, Animal& a2 )
{
    return ( a1.id >= a2.id );
}
