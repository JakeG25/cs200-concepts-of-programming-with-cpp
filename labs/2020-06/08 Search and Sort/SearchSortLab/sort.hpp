#ifndef _SORT_HPP
#define _SORT_HPP

#include <vector>
#include <string>
using namespace std;

#include "animal.hpp"

void InsertionSort( vector<Animal>& arr );
void SelectionSort( vector<Animal>& arr );
void MergeSort( vector<Animal>& arr, int left, int right );
void Merge( vector<Animal>& arr, int left, int mid, int right );

void Swap( Animal & a, Animal & b );

#endif

