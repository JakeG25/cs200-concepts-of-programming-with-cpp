#ifndef _DIE_HPP
#define _DIE_HPP

#include <cstdlib>

struct Die
{
    Die();
    Die( int sideCount );
    int Roll();

    int sides;
};

#endif
