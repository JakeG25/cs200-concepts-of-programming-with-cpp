#ifndef _STUDENT_HPP
#define _STUDENT_HPP

#include <string>
using namespace std;

class Student
{
    public:
    Student();

    void Setup( string newName );
    void AddGrade( float score );
    float GetGpa();
    string GetName();
    void Display();

    private:
    string fullName;
    float grades[10];
    int totalGrades;
};

#endif
