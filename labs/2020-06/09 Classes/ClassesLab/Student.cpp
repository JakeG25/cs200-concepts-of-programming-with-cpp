#include "Student.hpp"

#include <iostream>
using namespace std;

Student::Student()
{
    totalGrades = 0;
}

void Student::Setup( string newName )
{
    fullName = newName;
}

void Student::AddGrade( float score )
{
    if ( score < 0 || score > 4 )
    {
        cout << "Out of range!" << endl;
        return;
    }

    if ( totalGrades == 10 )
    {
        cout << "Grades array is full!" << endl;
        return;
    }

    grades[ totalGrades ] = score;
    totalGrades++;
}

float Student::GetGpa()
{
    float total = 0;
    for ( int i = 0; i < totalGrades; i++ )
    {
        total += grades[i];
    }
    return total / totalGrades;
}

string Student::GetName()
{
    return fullName;
}

void Student::Display()
{
    cout << GetName() << ": " << GetGpa() << endl;
}
