#ifndef _MOVIE_LIBRARY_HPP
#define _MOVIE_LIBRARY_HPP

#include <string>
using namespace std;

class MovieLibrary
{
    public:
    MovieLibrary();
    ~MovieLibrary();

    void ViewAllMovies() const;
    void ClearAllMovies();
    void UpdateMovie( int index, string newTitle );
    void AddMovie( string newTitle );
    int GetMovieCount();

    private:
    bool IsFull();
    void Resize();

    void Save();
    void Load();

    string * m_movieArray;
    int m_arraySize;
    int m_itemsStored;
};

#endif
